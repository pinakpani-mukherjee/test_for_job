import sys
file_name = sys.argv[1]
with open(file_name,"r") as f:
    lines = f.readlines()
    
test_num = int(lines[-1])

rel = []

for i in range(len(lines)-1):
    temp_num, temp_word = lines[i].split(":")
    temp_word = temp_word.strip()
    rel.append([int(temp_num),temp_word])
    
    
sorted_rel = sorted(rel,key=lambda l:l[0])

cnt = 0
for i in range(len(sorted_rel)):
    if test_num % sorted_rel[i][0] == 0:
        cnt += 1
        print(sorted_rel[i][1],end = "")
    if cnt == 0:
        print(test_num)